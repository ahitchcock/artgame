﻿using UnityEngine;
using System.Collections;

public class TankAttack : MonoBehaviour {
	private Collider coll;
	public float rocketCooldown = 5.0f;
	private float rocketCooldownCurrent = 5.0f;
	public GameObject rocket;
	private GameObject player;
    private bool isPaused;

	// Use this for initialization
	void Start () {
		rocketCooldownCurrent = Random.Range(0, rocketCooldown);
		coll = GetComponent<Collider> ();
		player = GameObject.FindWithTag ("Player");
        isPaused = false;
	}

	// Update is called once per frame
	void Update () {
        if(!isPaused)
        {
            rocketCooldownCurrent -= Time.deltaTime;
            if (rocketCooldownCurrent <= 0)
            {
                Vector3 pos = gameObject.transform.position;
                pos.y += 1f;
                pos += gameObject.transform.forward * 2.0f;

                Vector3 target = player.transform.position;
                target.y += 1.0f;

                GameObject cloneRocket = (GameObject)Instantiate(rocket, pos, Quaternion.Euler(player.transform.position - transform.position));
                cloneRocket.transform.LookAt(target);
                rocketCooldownCurrent = Random.Range(0, rocketCooldown);
            }
        }
	}

    public void pauseAttacking()
    {
        isPaused = true;
    }

    public void resumeAttacking()
    {
        isPaused = false;
    }
}
