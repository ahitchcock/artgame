﻿using UnityEngine;
using System.Collections;

public class BuildingScript : MonoBehaviour {

	public float maxHealth;
	public float currentHealth;

	public virtual void onBuildingDestroyed(){
        // Increment the current casualty value
        CasualtyHandler.addCasualty(1);

		Destroy (this.gameObject);
	}

	public virtual void onBuildingDamage(float damage){
		if(currentHealth <= 0) onBuildingDestroyed();
	}

	// Use this for initialization
	void Start () {
    }

	// Update is called once per frame
	void Update () {
		if(currentHealth <= 0) onBuildingDestroyed();
	}

	void OnTriggerEnter(Collider collision) {
		Debug.Log ("Collided");
		Projectile proj = collision.gameObject.GetComponent<Projectile> ();
		if (proj != null) {
			this.currentHealth -= proj.projectileDamage;
		}
	}
}
