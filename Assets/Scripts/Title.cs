﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Title : MonoBehaviour {
    private Canvas quitCanvas;
    private Canvas optionsCanvas;
    private Button startButton;
    private Button quitButton;
    private Button optionsButton;
    private Slider volumeSlider;
    private Text titleText;
    private AudioSource yesAudio;
    private AudioSource noAudio;

    // Use this for initialization
    void Start()
    {
        // Initialize variables
        quitCanvas = GameObject.Find("quitCanvas").GetComponent<Canvas>();
        optionsCanvas = GameObject.Find("optionsCanvas").GetComponent<Canvas>();
        startButton = GameObject.Find("startText").GetComponent<Button>();
        quitButton = GameObject.Find("quitText").GetComponent<Button>();
        optionsButton = GameObject.Find("optionsText").GetComponent<Button>();
        volumeSlider = GameObject.Find("volumeSlider").GetComponent<Slider>();
        titleText = GameObject.Find("titleText").GetComponent<Text>();
        yesAudio = GameObject.Find("yesClickSource").GetComponent<AudioSource>();
        noAudio = GameObject.Find("noClickSource").GetComponent<AudioSource>();

        // Set up initial menu
        quitCanvas.gameObject.SetActive(false);
        optionsCanvas.gameObject.SetActive(false);

        // Allow the volume slider to affect the main volume of the game
        volumeSlider.onValueChanged.AddListener(delegate { adjustVolume(); });
    }

    public void adjustVolume()
    {
        AudioListener.volume = volumeSlider.value;
    }

    public void optionsPressed()
    {
        yesAudio.PlayOneShot(yesAudio.clip);
        optionsCanvas.gameObject.SetActive(true);
        startButton.gameObject.SetActive(false);
        quitButton.gameObject.SetActive(false);
        optionsButton.gameObject.SetActive(false);
        titleText.gameObject.SetActive(false);
    }

    public void exitOptionsPressed()
    {
        noAudio.PlayOneShot(noAudio.clip);
        optionsCanvas.gameObject.SetActive(false);
        startButton.gameObject.SetActive(true);
        quitButton.gameObject.SetActive(true);
        optionsButton.gameObject.SetActive(true);
        titleText.gameObject.SetActive(true);
    }

    public void quitPressed()
    {
        noAudio.PlayOneShot(noAudio.clip);
        quitCanvas.gameObject.SetActive(true);
        startButton.gameObject.SetActive(false);
        quitButton.gameObject.SetActive(false);
        optionsButton.gameObject.SetActive(false);
    }

    public void noPressed()
    {
        noAudio.PlayOneShot(noAudio.clip);
        quitCanvas.gameObject.SetActive(false);
        startButton.gameObject.SetActive(true);
        quitButton.gameObject.SetActive(true);
        optionsButton.gameObject.SetActive(true);
        optionsButton.gameObject.SetActive(true);
    }

    public void yesPressed()
    {
        yesAudio.PlayOneShot(yesAudio.clip);
        Application.Quit();
    }

    public void startPressed()
    {
        // load level (with fade)
        SceneManager.LoadScene("Level1");
        yesAudio.PlayOneShot(yesAudio.clip);
    }
}
