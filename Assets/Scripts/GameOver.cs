﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour {
    private Button quitButton;
    private Button tryAgainButton;
    private Canvas quitCanvas;
    private GameObject message;
    private AudioSource yesAudioSource;
    private AudioSource noAudioSource;

	// Use this for initialization
	void Start () {
        quitButton = GameObject.Find("quitButton").GetComponent<Button>();
        tryAgainButton = GameObject.Find("tryAgainButton").GetComponent<Button>();
        message = GameObject.Find("message");
        quitCanvas = GameObject.Find("quitCanvas").GetComponent<Canvas>();
        yesAudioSource = GameObject.Find("yesClickSource").GetComponent<AudioSource>();
        noAudioSource = GameObject.Find("noClickSource").GetComponent<AudioSource>();

        quitCanvas.gameObject.SetActive(false);
	}
	
	public void tryAgainPressed()
    {
        yesAudioSource.PlayOneShot(yesAudioSource.clip);
        SceneManager.LoadScene(1);
    }

    public void quitPressed()
    {
        noAudioSource.PlayOneShot(noAudioSource.clip);
        quitCanvas.gameObject.SetActive(true);
        quitButton.gameObject.SetActive(false);
        tryAgainButton.gameObject.SetActive(false);
        message.SetActive(false);
    }

    public void yesPressed()
    {
        yesAudioSource.PlayOneShot(yesAudioSource.clip);
        Application.Quit();
    }

    public void noPressed()
    {
        noAudioSource.PlayOneShot(noAudioSource.clip);
        quitCanvas.gameObject.SetActive(false);
        quitButton.gameObject.SetActive(true);
        tryAgainButton.gameObject.SetActive(true);
        message.SetActive(true);
    }
}
