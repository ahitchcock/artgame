﻿using UnityEngine;
using System.Collections;

public class MoveTest : MonoBehaviour {
    public Transform goal;
    private bool isPaused;

	// Use this for initialization
	void Start () {
        isPaused = false;  
	}
	
	// Update is called once per frame
	void Update () {
        if(!isPaused)
        {
            NavMeshAgent agent = GetComponent<NavMeshAgent>();
            agent.destination = goal.position;
        }    
    }

    public void stopMoving()
    {
        isPaused = true;
    }

    public void resumeMoving()
    {
        isPaused = false;
    }
}
