﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuHandle : MonoBehaviour {
    private Canvas HUD;
    private Canvas menu;
    private Canvas dialogue;
    private Canvas quitCanvas;
    private Text menuTitle;
    private Button quitButton;
    private Button titleReturnButton;
    private AudioSource dialogueAudio;
    private AudioSource yesClickSource;
    private AudioSource noClickSource;
    private PlayerController playerController;

	// Use this for initialization
	void Start () {
        // Initialize variables
        HUD = GameObject.Find("HUD").GetComponent<Canvas>();
        menu = GameObject.Find("MenuCanvas").GetComponent<Canvas>();
        quitCanvas = GameObject.Find("quitCanvas").GetComponent<Canvas>();
        dialogue = GameObject.FindGameObjectWithTag("DialogueCanvas").GetComponent<Canvas>();
        menuTitle = GameObject.Find("MenuTitle").GetComponent<Text>();
        quitButton = GameObject.Find("quitButton").GetComponent<Button>();
        titleReturnButton = GameObject.Find("titleReturnButton").GetComponent<Button>();
        dialogueAudio = GameObject.Find("dialogueAudioSource").GetComponent<AudioSource>();
        yesClickSource = GameObject.Find("yesClickSource").GetComponent<AudioSource>();
        noClickSource = GameObject.Find("noClickSource").GetComponent<AudioSource>();
        playerController = GameObject.Find("Player").GetComponent<PlayerController>();

        // Don't show the menu at first
        menu.gameObject.SetActive(false);
        quitCanvas.gameObject.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.M))
        {
            if(HUD.gameObject.activeSelf)
            {
                HUD.gameObject.SetActive(false);
                dialogue.gameObject.SetActive(false);
                menu.gameObject.SetActive(true);

                if (dialogueAudio.isPlaying)
                    dialogueAudio.Pause();

                // Freeze any enemies
                GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");

                for(int i = 0; i < enemies.Length; i++)
                {
                    enemies[i].GetComponent<TankAttack>().pauseAttacking();
                    enemies[i].GetComponent<MoveTest>().stopMoving();
                }

                // Freeze the player
                playerController.pauseMovement();
            }
            else
            {
                HUD.gameObject.SetActive(true);
                dialogue.gameObject.SetActive(true);
                menu.gameObject.SetActive(false);

                dialogueAudio.UnPause();

                // Resume enemy movement and attack
                GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");

                for(int i = 0; i < enemies.Length; i++)
                {
                    enemies[i].GetComponent<TankAttack>().resumeAttacking();
                    enemies[i].GetComponent<MoveTest>().resumeMoving();
                }

                // Allow player to move again
                playerController.resumeMovement();
            }
        }
	}

    public void returnToTitle()
    {
        noClickSource.PlayOneShot(noClickSource.clip);
        SceneManager.LoadScene("TitleScreen");
    }

    public void quitPressed()
    {
        noClickSource.PlayOneShot(noClickSource.clip);
        quitCanvas.gameObject.SetActive(true);
        menu.gameObject.SetActive(false);
        menuTitle.gameObject.SetActive(false);
        titleReturnButton.gameObject.SetActive(false);
        quitButton.gameObject.SetActive(false);
    }

    public void noPressed()
    {
        noClickSource.PlayOneShot(noClickSource.clip);
        quitCanvas.gameObject.SetActive(false);
        menu.gameObject.SetActive(true);
        menuTitle.gameObject.SetActive(true);
        titleReturnButton.gameObject.SetActive(true);
        quitButton.gameObject.SetActive(true);
    }
    
    public void yesPressed()
    {
        yesClickSource.PlayOneShot(yesClickSource.clip);
        Application.Quit();
    }
}
