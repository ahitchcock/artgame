﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerDamageHandler : MonoBehaviour {
    public static int maxHP;
    public static int maxShieldValue;
    private static int startingMaxHP;
    private static int startingmaxShieldValue;
    public float playerDefense;
    private float currentHP;
    private float currentShieldValue;
    private bool isHPRegenPaused;

    private Slider healthBar;
    private Image healthFillImage;
    private AudioSource healthAudioSource;
    private Slider shieldBar;
    private Image shieldFillImage;

    public AudioClip damageClip;

	// Use this for initialization
	void Start () {
        // Set default values if maxHP/maxShield values are not supplied
        if (maxHP <= 0)
            maxHP = 100;
        if (maxShieldValue <= 0)
            maxShieldValue = 10;

        // Set up health variables
        healthFillImage = GameObject.Find("healthFill").GetComponent<Image>();
        healthBar = GameObject.Find("healthSlider").GetComponent<Slider>();
        healthAudioSource = GameObject.Find("healthAudioSource").GetComponent<AudioSource>();
        currentHP = maxHP;
        startingMaxHP = maxHP;
        startingmaxShieldValue = maxShieldValue;
        healthBar.maxValue = maxHP;
        healthBar.value = maxHP;
        isHPRegenPaused = false;

        // Set up shield variables
        shieldBar = GameObject.Find("shieldSlider").GetComponent<Slider>();
        shieldFillImage = GameObject.Find("shieldFill").GetComponent<Image>();
        currentShieldValue = maxShieldValue;
        shieldBar.maxValue = maxShieldValue;
        shieldBar.value = maxShieldValue;


        InvokeRepeating("backgroundRecovery", 0, 1);
    }
	
	// Update is called once per frame
	void Update () {
        
	}

    public void recoverShield(int recoverValue)
    {
        if(!isHPRegenPaused)
        {
            currentShieldValue += recoverValue;

            if (currentShieldValue > maxShieldValue)
                currentShieldValue = maxShieldValue;

            shieldBar.value = currentShieldValue;
        }
    }

    private void backgroundRecovery()
    {
        if (currentHP < maxHP)
            recoverHP(1);
    }

    public void recoverHP(int recoverHP)
    {
        currentHP += recoverHP;

        if (currentHP > maxHP)
            currentHP = maxHP;

        healthBar.value = currentHP;

        updateHPColors();
    }

    public void pauseHPRecover()
    {
        isHPRegenPaused = true;
    }

    public void resumeHPRecover()
    {
        isHPRegenPaused = false;
    }

    public static void resetHealthShield()
    {
        maxHP = startingMaxHP;
        maxShieldValue = startingmaxShieldValue;
    }

    public void takeWeightedDamage(int attack)
    {
        float leftoverDamage = 0;

        // Allow the attack to affect the shields first
        if (currentShieldValue > 0)
        {
            currentShieldValue -= attack;

            if (currentShieldValue < 0)
            {
                leftoverDamage = -currentShieldValue;
                currentShieldValue = 0;
            }

            shieldBar.value = currentShieldValue;
        }
        else
            leftoverDamage = attack;

        float weightedDamage = ((1 - playerDefense) * leftoverDamage);
        takeSetDamage(weightedDamage);
    }

    public void takeSetDamage(float damage)
    {
        currentHP -= damage;
        if (currentHP < 0)
            currentHP = 0;

        healthBar.value = currentHP;

        healthAudioSource.clip = damageClip;
        healthAudioSource.Play();

        updateHPColors();

        // Game Over!
        if (currentHP <= 0)
        {
            // Switch to game over scene and reset maxHP/maxShield values
            resetHealthShield();
            SceneManager.LoadScene("GameOverDead");
        }
    }

    private void updateHPColors()
    {
        if (currentHP < (0.125 * maxHP))
        {
            healthFillImage.color = new Color(0.816f, 0.234f, 0.234f, 1);
        }
        else if (currentHP < (0.5 * maxHP))
        {
            healthFillImage.color = new Color(0.964f, 1, 0.346f, 1);
        }
        else
        {
            healthFillImage.color = new Color(0.234f, 0.816f, 0.262f, 1);
        }
    }
}
