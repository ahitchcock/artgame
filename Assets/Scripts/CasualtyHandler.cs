﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CasualtyHandler : MonoBehaviour {
    private static Slider casualtySlider;

	// Use this for initialization
	void Start () {
        // Initialize variables
        casualtySlider = GameObject.Find("casualtySlider").GetComponent<Slider>();

        // Count all enemy/building objects
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
        GameObject[] buildings = GameObject.FindGameObjectsWithTag("Building");

        casualtySlider.maxValue = enemies.Length + buildings.Length;
        casualtySlider.value = 0;
	}

    public static void addCasualty(int numCasualties)
    {
        casualtySlider.value += numCasualties;

        // Level complete! Switch scenes here~
        if(casualtySlider.value >= casualtySlider.maxValue)
        {
            SceneHandler.switchScene();
        }

    }
}
