﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

	public float speed = 10.0f;

	// Use this for initialization
	void Start () {

	}

	private float elapsedTime = 0;

	// Update is called once per frame
	void Update () {
		//Debug.Log (elapsedTime);
		elapsedTime += Time.deltaTime;

		this.GetComponent<Rigidbody> ().AddForce (transform.forward * speed);
		if (elapsedTime > 5)
			Destroy (gameObject);
	}

	void OnTriggerEnter(Collider collision) {
		Destroy (this.gameObject, 1.0f);
	}
}
