﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class dialogueHandler : MonoBehaviour {
    private Canvas dialogueCanvas;
    private Canvas menuCanvas;
    private Image dialogueBack;
    private AudioSource dialogueAudioSource;
    private Text dialogueText;
    public List<string> dialogueStrings;
    public List<AudioClip> audioClips;
    public bool automaticallyBegin;
    private bool isAudioPlaying;
    private bool isOngoing;
    private bool waitingForInput;

    // Use this for initialization
    void Start () {
        dialogueCanvas = GameObject.FindGameObjectWithTag("DialogueCanvas").GetComponent<Canvas>();
        dialogueBack = GameObject.Find("dialogueBack").GetComponent<Image>();
        dialogueAudioSource = GameObject.Find("dialogueAudioSource").GetComponent<AudioSource>();
        dialogueText = GameObject.Find("dialogueText").GetComponent<Text>();

        if (automaticallyBegin && (dialogueStrings.Count > 0 || audioClips.Count > 0))
        {
            isOngoing = true;

            // Display first message if it exists
            if(dialogueStrings.Count > 0)
            {
                if (audioClips.Count == 0)
                    waitingForInput = true;
                dialogueBack.enabled = true;
                dialogueText.enabled = true;
                dialogueText.text = dialogueStrings[0];
                dialogueStrings.RemoveAt(0);
            }
            else
                dialogueBack.enabled = false;

            // Play first audio clip if it exists
            if(audioClips.Count > 0)
            {
                dialogueAudioSource.clip = audioClips[0];
                dialogueAudioSource.Play();
                audioClips.RemoveAt(0);         
            }
        }   
        else
        {
            isOngoing = false;
            dialogueBack.enabled = false;
            dialogueText.enabled = false;
        }           
	}

    public void beginDialogue()
    {
        if (audioClips.Count > 0 || dialogueStrings.Count > 0)
        {
            isOngoing = true;

            // Display first message if it exists
            if (dialogueStrings.Count > 0)
            {
                dialogueBack.enabled = true;
                dialogueText.enabled = true;
                dialogueText.text = dialogueStrings[0];
                dialogueStrings.RemoveAt(0);
            }
            else
                dialogueBack.enabled = false;

            // Play first audio clip if it exists
            if (audioClips.Count > 0)
            {
                dialogueAudioSource.clip = audioClips[0];
                dialogueAudioSource.Play();
                audioClips.RemoveAt(0);
            }
        }
    }
	
	// Update is called once per frame
	void Update () {
	    if(isOngoing && Input.GetKeyDown(KeyCode.Return) || (isOngoing && !dialogueAudioSource.isPlaying && !waitingForInput)) //&& !menuCanvas.enabled)
        {
            if (dialogueBack.enabled == true && dialogueStrings.Count == 0)
            {
                dialogueBack.enabled = false;
                dialogueText.enabled = false;
                waitingForInput = false;
            }
                
            if (dialogueStrings.Count == 0 && audioClips.Count == 0)
            {
                isOngoing = false;
                dialogueBack.enabled = false;
                dialogueText.enabled = false;
                waitingForInput = false;
                if (dialogueAudioSource.isPlaying)
                    dialogueAudioSource.Stop();
            }               

            if(dialogueStrings.Count > 0)
            {
                if (audioClips.Count == 0)
                    waitingForInput = true;

                dialogueText.text = dialogueStrings[0];
                dialogueStrings.RemoveAt(0);
            }
            
            if(audioClips.Count > 0)
            {
                dialogueAudioSource.clip = audioClips[0];
                dialogueAudioSource.Play();
                audioClips.RemoveAt(0);
            }
        }
	}

    public void addDialogue(AudioClip clip)
    {
        audioClips.Add(clip);
    }

    public void addDialogue(string dialogueText)
    {
        dialogueStrings.Add(dialogueText);
    }

    public void addDialogue(AudioClip clip, string dialogueText)
    {
        audioClips.Add(clip);
        dialogueStrings.Add(dialogueText);
    }
}
