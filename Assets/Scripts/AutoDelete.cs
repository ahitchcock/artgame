﻿using UnityEngine;
using System.Collections;

public class AutoDelete : MonoBehaviour {

	public float secondsUntilDelete = 2.0f; 

	// Use this for initialization
	void Start () {
		Destroy (gameObject, secondsUntilDelete);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
