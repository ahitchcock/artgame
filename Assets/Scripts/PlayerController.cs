﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	public float moveSpeed = 10.0f;
	public float bulletCooldown = .1f;
	public GameObject rocket;
	public GameObject bullet;
	private PlayerDamageHandler damageHandler;
    private bool isPaused;

	private CharacterController controller;
	// Use this for initialization
	void Start () {
		controller = this.GetComponent<CharacterController> ();
		damageHandler = this.GetComponent<PlayerDamageHandler> ();
        isPaused = false;
	}

	private float bulletTimer = 0.0f;

	void OnTriggerEnter(Collider collision){
		Projectile proj = collision.gameObject.GetComponent<Projectile> ();
        if (proj.owner != gameObject)
            damageHandler.takeWeightedDamage (proj.projectileDamage);
	}

	// Update is called once per frame
	void Update () {
        // Stop character movement if the player is paused
        if (isPaused)
            return;

		Vector3 move = new Vector3 (0f, 0f, 0f);
		move.x = Input.GetAxis ("Horizontal");
		move.y = Input.GetAxis ("Raise");
		move.z = Input.GetAxis ("Vertical");

		move = transform.TransformDirection (move);
		move.Normalize ();

		float rotateX = Input.GetAxis ("Mouse X");
		float rotateY = Input.GetAxis ("Mouse Y");
		transform.Rotate(new Vector3(0, rotateX, 0));
		Vector3 noZ = transform.eulerAngles;
		noZ.z = 0;
		transform.eulerAngles = noZ;

		RectTransform sprite = GameObject.FindWithTag ("Crosshair").GetComponent<RectTransform>();
		sprite.position = Input.mousePosition;

		Ray ray = Camera.main.ScreenPointToRay(sprite.position);
		Vector3 modifiedOrigin = ray.GetPoint (2.0f);

		if (Input.GetMouseButtonDown (0)) {
			Vector3 pos = gameObject.transform.position;
			pos.y += 2f;
			pos += gameObject.transform.forward * 2.0f;

			GameObject cloneRocket = (GameObject)Instantiate (rocket, pos, Quaternion.FromToRotation(Vector3.forward, Camera.main.ScreenPointToRay(sprite.position).direction));

            cloneRocket.GetComponent<Projectile>().owner = gameObject;
			RaycastHit hit;
			if (Physics.Raycast (ray, out hit)) {
				cloneRocket.transform.LookAt (hit.point);
				Debug.Log (hit.point);
				//gameObject.transform.position = hit.point;
			}
			//cloneRocket.transform.parent = this.transform;
		}


		bulletTimer -= Time.deltaTime;
		bulletTimer = Mathf.Max (bulletTimer, 0);
		if (Input.GetMouseButton (1)) {
			if (bulletTimer <= 0) {
				Vector3 pos = gameObject.transform.position;
				pos.y += 2f;
				pos += gameObject.transform.forward * 2.0f;

				//rot.y += 90;
				GameObject cloneBullet = (GameObject)Instantiate (bullet, pos, Quaternion.FromToRotation(Vector3.forward, Camera.main.ScreenPointToRay(sprite.position).direction));

                cloneBullet.GetComponent<Projectile>().owner = gameObject;
                RaycastHit hit;
				if (Physics.Raycast (ray, out hit)) {
					cloneBullet.transform.LookAt (hit.point);
					Debug.Log (hit.point);
					//gameObject.transform.position = hit.point;
				}
				bulletTimer = bulletCooldown;
			}
		}

		//Camera.main.transform.Rotate( new Vector3(-rotateY, 0,0));

		controller.Move (move * moveSpeed);

		//Debug.Log (move);
	}

    public void pauseMovement()
    {
        isPaused = true;
    }

    public void resumeMovement()
    {
        isPaused = false; 
    }
}
