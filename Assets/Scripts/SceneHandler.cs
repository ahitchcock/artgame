﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class SceneHandler : MonoBehaviour {
	// Use this for initialization
	void Start () {}
	
    public static void switchScene()
    {
        int levelNum = int.Parse(SceneManager.GetActiveScene().name.Substring(5));

        // Switch scenes if not at end of game
        if(levelNum < 3)
        {
            // Increase player's max HP/Shield before switching
            PlayerDamageHandler.maxHP += 10;
            PlayerDamageHandler.maxShieldValue += 5;
            string nextLevel = "Level" + (1 + levelNum);
            Debug.Log("Next level is: " + nextLevel);

            SceneManager.LoadScene(nextLevel);
        }
        else
        {
            // End of game. Go to end game scene and reset stats
            PlayerDamageHandler.resetHealthShield();
            SceneManager.LoadScene("GameOverWin");
        }
    }
}
