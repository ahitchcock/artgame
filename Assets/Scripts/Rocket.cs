﻿using UnityEngine;
using System.Collections;

public class Rocket : MonoBehaviour {

	public float rocketSpeed = 10.0f;
	public ParticleSystem explosion;

	// Use this for initialization
	void Start () {
		
	}

	private float elapsedTime = 0;
	
	// Update is called once per frame
	void Update () {
		//Debug.Log (elapsedTime);
		elapsedTime += Time.deltaTime;
		this.GetComponent<Rigidbody> ().AddForce (gameObject.transform.forward * rocketSpeed);
		if (elapsedTime > 10)
			Destroy (gameObject);
	}

	void OnTriggerEnter(Collider collision) {
		ParticleSystem exp = (ParticleSystem)Instantiate (explosion, transform.position, transform.rotation);
		Destroy (exp, 2.0f);
		Destroy (this.gameObject, .01f);
	}
}
