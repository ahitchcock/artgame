﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

	public float maxHealth;
	public float currentHealth;
	



	public virtual void onEnemyDie(){
        // Increment the current casualty value
        CasualtyHandler.addCasualty(1);

        Destroy (this.gameObject);
	}

	public virtual void onEnemyDamage(float damage){
		if(currentHealth <= 0) onEnemyDie();
	}
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(currentHealth <= 0) onEnemyDie();
	}

	void OnTriggerEnter(Collider collision) {
		Projectile proj = collision.gameObject.GetComponent<Projectile> ();
		if (proj != null) {
			this.currentHealth -= proj.projectileDamage;
		}

	}
}
